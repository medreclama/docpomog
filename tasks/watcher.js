import gulp from 'gulp';
import watch from 'gulp-watch';
import browserSync from 'browser-sync';

const { reload } = browserSync;

gulp.task('reload', (done) => {
  reload();
  done();
});

gulp.task('watcher', (done) => {
  global.isWatching = true;
  watch([
    './src/blocks/**/*.styl',
    './src/common/stylus/**/*.styl',
    './src/pages/**/*.styl',
  ], gulp.series('stylus', 'reload'));
  watch([
    './src/blocks/**/*.pug',
    './src/common/pug/**/*.pug',
    './src/data/**/*.pug',
    './src/pages/**/*.pug',
  ], gulp.series('template', 'reload'));
  watch([
    './src/blocks/**/*.js',
    './src/common/js/**/*.js',
    './src/pages/**/*.js',
  ], gulp.series('script', 'reload'));
  watch('./src/resources/**', gulp.series('copy', 'reload'));
  watch('./src/resources/images/**', gulp.series('webp', 'reload'));
  watch('./src/svg-sprites/**', gulp.series('svgSprites', 'reload'));
  done();
});
