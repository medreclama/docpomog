import { homeConsultationInit } from '../../blocks/home-consultation/home-consultation';

window.addEventListener('DOMContentLoaded', () => {
  homeConsultationInit();
});
