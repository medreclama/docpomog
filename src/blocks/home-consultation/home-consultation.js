const homeConsultation = document.querySelector('.home-consultation');
export const homeConsultationInit = () => {
  const homeConsultationButton = homeConsultation.querySelector('.home-consultation__button');
  homeConsultationButton.addEventListener('click', (evt) => {
    evt.preventDefault();
    homeConsultation.classList.toggle('home-consultation--active');
  });
};
